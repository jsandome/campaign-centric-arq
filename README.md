# Documentation

## Run slate with docker
```Console
$: docker run --rm --name slate -p 4567:4567 -v $pwd/source:/srv/slate/source slatedocs/slate serve
```

## Open Documentation 
[Link](http://localhost:4567/)
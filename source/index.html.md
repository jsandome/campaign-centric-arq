---
title: API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  + typescript

toc_footers:
  + <a href='#'>Sign Up for a Developer Key</a>
  + <a href='https://github.com/slatedocs/slate'>Documentation Powered by Slate</a>

includes:
  + errors

search: true4

code_clipboard: true

meta:
  + name: description
    content: Documentation for the Kittn API

---

# Introduction

Welcome to the **ARGO bot documentation**! 

In this section you will find information about how to integrate **ARGO bot API**.

# Authentication

**Not required**

# Visa type

## Get All visa types

```typescript
export interface IVisaTypeDto {
    id:string;
    description:string;
}

// GET All visa types
getVisaTypes(): Observable<IVisaTypeDto[]>{
   const params = new HttpParams();
   return this.httpClient.request(
     'GET', 
     `${this.urlBase}/visatype`, 
     {responseType:'json'});
}
```

> The above command returns JSON structured like this:

```json
[
  {
    "id": "6152885da4a9bfe8c4313093",
    "description": "Business/Tourism"
  },
  {
    "id": "6152885da4a9bfe8c4313094",
    "description": "Work"
  },
  {
    "id": "6152885da4a9bfe8c4313095",
    "description": "Student"
  }
]
```

This endpoint retrieves all visa types.

### HTTP Request

`GET https://development-api-argobot.umvel.dev/api/visatype`     
`GET https://staging-api-argobot.umvel.dev:3002/api/visatype`    
`GET https://demo-api-argobot.umvel.dev:3003/api/visatype`   


### Query Parameters

**None**

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api)   

<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Embassy

## Get All embassies 

```typescript
export class Embassy {
    id:string;
    countryId: string;
    name: string;
}

// GET All embassies
getEmbasies(): Observable<Embassy[]>{
   const params = new HttpParams();
   return this.httpClient.request(
     'GET', 
     `${this.urlBase}/embassy`, 
     {responseType:'json'});
}
```

> The above command returns JSON structured like this:

```json
[
  {
    "id": "615535e1dbcb87c08e2410f1",
    "name": "U.S. Embassy Havana",
    "countryId": "61546a29dbcb87c08e240e7f"
  },
  {
    "id": "615535e1dbcb87c08e2410c1",
    "name": "U.S. Consulate in Sydney",
    "countryId": "61546a29dbcb87c08e240e54"
  },
  {
    "id": "615535e1dbcb87c08e241102",
    "name": "U.S. Embassy in Finland",
    "countryId": "61546a29dbcb87c08e240e91"
  },
  {
    "id": "615535e1dbcb87c08e241129",
    "name": "U.S. Embassy in Tehran",
    "countryId": "61546a29dbcb87c08e240eaf"
  }
]
```

This endpoint retrieves all embassies.

### HTTP Request

`GET https://development-api-argobot.umvel.dev/api/embassy`     
`GET https://staging-api-argobot.umvel.dev:3002/api/embassy`    
`GET https://demo-api-argobot.umvel.dev:3003/api/embassy`   

### Query Parameters

**None**

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api)   

<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Country

## Get All contries 

```typescript
export class Country {
    id:string;
    name: string;
    countryCode: string;
}


// GET All countries
getCountries(): Observable<Country[]>{
   const params = new HttpParams();
   return this.httpClient.request(
     'GET', 
     `${this.urlBase}/country`, 
     {responseType:'json'});
}
```

> The above command returns JSON structured like this:

```json
[
  {
    "id": "61546a29dbcb87c08e240e50",
    "name": "Antigua and Barbuda",
    "countryCode": "AG"
  },
  {
    "id": "61546a29dbcb87c08e240e55",
    "name": "Austria",
    "countryCode": "AT"
  },
  {
    "id": "61546a29dbcb87c08e240e6b",
    "name": "Burundi",
    "countryCode": "BI"
  }
]
```

This endpoint retrieves all countries.

### HTTP Request

`GET https://development-api-argobot.umvel.dev/api/visatype`     
`GET https://staging-api-argobot.umvel.dev:3002/api/visatype`    
`GET https://demo-api-argobot.umvel.dev:3003/api/visatype`   

### Query Parameters

**None**

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 

<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Ocuppation

## Get All ocuppations

```typescript
export class Occupation {
    id:string;
    description: string;
}


// GET All countries
getOcuppations(): Observable<IOcuppation[]>{
   const params = new HttpParams();
   return this.httpClient.request(
     'GET', 
     `${this.urlBase}/ocuppation`, 
     {responseType:'json'});
}
```

> The above command returns JSON structured like this:

```json
[
  {
    "id": "6154730ddbcb87c08e240f45",
    "description": "Accountant"
  },
  {
    "id": "6154730ddbcb87c08e240f50",
    "description": "Bartender"
  },
  {
    "id": "6154730ddbcb87c08e240f51",
    "description": "Bookseller"
  }
]
```

This endpoint retrieves all countries.

### HTTP Request

`GET https://development-api-argobot.umvel.dev/api/occupation`     
`GET https://staging-api-argobot.umvel.dev:3002/api/occupation`    
`GET https://demo-api-argobot.umvel.dev:3003/api/occupation`  

### Query Parameters

**None**

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 

<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Degree of study

## Get All degrees

```typescript

export class Degree {
    id:string;
    description: string;
}


// GET All countries
getDegrees(): Observable<Degree[]>{
   const params = new HttpParams();
   return this.httpClient.request(
     'GET', 
     `${this.urlBase}/degree`, 
     {responseType:'json'});
```

> The above command returns JSON structured like this:

```json
[
  {
    "id": "615488b2dbcb87c08e240faf",
    "description": "Master"
  },
  {
    "id": "615488b2dbcb87c08e240fb0",
    "description": "Doctoral"
  },
  {
    "id": "615488b2dbcb87c08e240fad",
    "description": "Associate"
  },
  {
    "id": "615488b2dbcb87c08e240fae",
    "description": "Bachelor"
  }
]
```

This endpoint retrieves all countries.

### HTTP Request

`GET https://development-api-argobot.umvel.dev/api/degree`     
`GET https://staging-api-argobot.umvel.dev:3002/api/degree`    
`GET https://demo-api-argobot.umvel.dev:3003/api/degree`  


### Query Parameters

**None**

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 

<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Application

## POST application 

```typescript

export class ApplicationResponse {
   id:string;
   createdAt:string;
   token:string;
   identifierFileCredentials: S3Credentials;
   photoFileCredentials: S3Credentials;
}

export class S3Credentials {
   url:string;
   fields: any;
}

// POST application
postApplication(): Observable<ApplicationResponse[]>{
   const params = {
     visaTypeId: "615488b2dbcb87c08e240fae" ,
     embassyId: "615488b2dbcb87c08e240fae",
     previuosDenied:false,
     visitedCountryIds: [
       "615488b2dbcb87c08e240fae","615488b2dbcb87c08e240fae","615488b2dbcb87c08e240fae","615488b2dbcb87c08e240fae"
     ],
     firstName:"MyName",
     lastName:"MyLastName",
     gender: "Male", 
     ageRange:"30-39",
     residenceCountryId : "615488b2dbcb87c08e240fae",
     maritalStatus: "MARRIED", 
     maxDegreeId: "615488b2dbcb87c08e240fae" ,
     occupationId: "615488b2dbcb87c08e240fae" ,
     identifierFileExtension:".jpg",
     photoFileExtension :".png"
   }

   return this.httpClient.request(
     'POST', 
     `${this.urlBase}/application`, 
     {responseType:'json'});
  }
```

> The above command returns JSON structured like this:

```json
//OK
{
  "id": "61546a29dbcb87c08e240e50",
  "createdAt": "1633484367",
  "identifierFileCredentials": {
    "url": "https://s3.us-west-1.amazonaws.com/argo-bot-be-dev",
    "fields": {
      "bucket": "argo-bot-be-dev",
      "X-Amz-Algorithm": "AWS4-HMAC-SHA256",
      "X-Amz-Credential": "AKIAUJVA7V8PQM4VZAOY/20211014/us-west-1/s3/aws4_request",
      "X-Amz-Date": "20211014T064543Z",
      "key": "6167d2176add94bf9a550af9/6c8bcdce-5537-461f-ad9e-c382c10435b6.jpg",
      "Policy": "eyJleHBpcmF0aW9uILoiMjAyMS0xMC0xNFQwNzo0NTo0M1oiLCJjb25kaXRpb25zIjpbeyJidWNrZXQiOiJ0ZXN0LXByZXNpZ25lZC11cmxzLW14In0seyJYLUFtei1BbGdvcml0aG0iOiJBV1M0LUhNQUMtU0hBMjU2In0seyJYLUFtei1DcmVkZW50aWFsIjoiQUtJQVVKVkE3VlRQUU00VlpBT1gvMjAyMTEwMTQvdXMtd2VzdC0xL3MzL2F3czRfcmVxdWVzdCJ9LHsiWC1BbXotRGF0ZSI6IjIwMjExMDE0VDA2NDU0M1oifSx7ImtleSI6IjYxNjdkMjE3NmFkZDk0YmY5YTU1MGFmOS82YzhiY2RjZS01NTM3LTQ2MWYtYWQ5ZS1jMzgyYzEwNDM1YjYuanBnIn1dfQ==",
      "X-Amz-Signature": "436alcd2d5775f555cdd9da316531b282dea95e0b4ea908e902b36dd84f5cb94"
    }
  },
  "photoFileCredentials": {
    "url": "https://s3.us-west-1.amazonaws.com/argo-bot-be-dev",
    "fields": {
      "bucket": "argo-bot-be-dev",
      "X-Amz-Algorithm": "AWS4-HMAC-SHA256",
      "X-Amz-Credential": "AKIAUJVA7V8PQM4VZAOY/20211014/us-west-1/s3/aws4_request",
      "X-Amz-Date": "20211014T064543Z",
      "key": "6167d2176add94bf9a550af9/6c8bcdce-5537-461f-ad9e-c382c10435b6.jpg",
      "Policy": "eyJleHBpcmF0aW9uILoiMjAyMS0xMC0xNFQwNzo0NTo0M1oiLCJjb25kaXRpb25zIjpbeyJidWNrZXQiOiJ0ZXN0LXByZXNpZ25lZC11cmxzLW14In0seyJYLUFtei1BbGdvcml0aG0iOiJBV1M0LUhNQUMtU0hBMjU2In0seyJYLUFtei1DcmVkZW50aWFsIjoiQUtJQVVKVkE3VlRQUU00VlpBT1gvMjAyMTEwMTQvdXMtd2VzdC0xL3MzL2F3czRfcmVxdWVzdCJ9LHsiWC1BbXotRGF0ZSI6IjIwMjExMDE0VDA2NDU0M1oifSx7ImtleSI6IjYxNjdkMjE3NmFkZDk0YmY5YTU1MGFmOS82YzhiY2RjZS01NTM3LTQ2MWYtYWQ5ZS1jMzgyYzEwNDM1YjYuanBnIn1dfQ==",
      "X-Amz-Signature": "436alcd2d5775f555cdd9da316531b282dea95e0b4ea908e902b36dd84f5cb94"
    }
  },
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjMyIiwiaWF0IjoxNjMzNDgzNTI2LCJleHAiOjE2MzM0ODM1ODZ9.6_EZjvsV9kFLKKNfC59H4R7CXlFVM6TZufXNZdYCyV0"
}

// 400 
{
  "statusCode": 400,
  "message": [
    "APPLICATION.VISATYPEID",
    "APPLICATION.EMBASSYID"
  ],
  "error": "Bad Request"
}

```

This endpoint save all information about application

### HTTP Request

`POST https://development-api-argobot.umvel.dev/api/application`     
`POST https://staging-api-argobot.umvel.dev:3002/api/application`    
`POST https://demo-api-argobot.umvel.dev:3003/api/application`  

### Gender

<ol>
<li>Male</li>
<li>Female</li>
<li>Other</li>
</ol>

### Marital Status 


<ol>
<li>Married</li>
<li>Widowed</li>
<li>Separated</li>
<li>Divorced</li>
<li>Single</li>
</ol>


### Age range

<ol>
<li>0-12</li>
<li>13-17</li>
<li>18-22</li>
<li>23-27</li>
<li>28-31</li>
<li>32-65</li>
<li>> 65</li>
</ol>

### Body Parameters

Parameter | Required | type | Description
--------- | ------- | --------|-----------
visaTypeId | true | string |Id of visa type from visa type catalog
embassyId | true | string |Id of embassy from embassies catalog
previuosDenied | true | boolean |Information about if person has a previous denied visa application
visitedCountryIds | false | string[] | IDs of visited countries 
firstName | true | string | first name
lastName | true | string | Last name
gender | true | string | Gender 
ageRange | true  | string | Age range
residenceCountryId | true | string | Country of residence 
maritalStatus | true | string | Marital status 
maxDegreeId  | true | string | Max degree 
occupationId | true | string | Occupation
identifierFileExtension | true | string | .png .jpg .jpeg .pdf etc..
photoFileExtension | true | string | .png .jpg .jpeg .pdf etc..

### Response 200

Parameter | type | Description
--------- | --------|-----------
id | string | ID of application this information will be useful to upload official document and start interview 
createdAt | number | Time of application is an EPOCH UNIX format
identifierFileCredentials | Object | URL with credentials to upload identifier
photoFileCredentials | string | URL with credentias to upload photo
token | string | token to perform call APIs
voiceType | string | Male or Female


### Error messages 400 BadRequest 

CODE  | Description
--------- |-----------
APPLICATION.VISATYPEID | Parameter visa type is required
APPLICATION.EMBASSYID | Parameter embassy is required
APPLICATION.FIRSTNAME | Parameter firstname is required
APPLICATION.LASTNAME | Parameter lastname is required 
APPLICATION.AGERANGE | Parameter age range is required or is invalid
APPLICATION.GENDER | Parameter gender is invalid 
APPLICATION.RESIDENCECOUNTRYID | Parameter residence country  is required
APPLICATION.MARITALSTATUS | Parameter Marital status is invalid
APPLICATION.MAXDEGREEID | Parameter degree is required
APPLICATION.OCUPPATIONID | Parameter ocuppation is required
APPLICATION.IDENTIFIERFILEEXTENSION | Parameter identifier file extension is required
APPLICATION.PHOTOFILEEXTENSION | Parameter photo file extension is required



### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 


<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Upload files

## POST Upload file to S3 bucket 

```typescript
//////////////////////// angular 8+ ////////////////////////////////////////////////////////////////////////////////////
///////////////////// use parameters from Application response //////////////////////////////////////////////////////////
                      this.fileForm.nativeElement.setAttribute("action", result.data.url)

                      const formData = new FormData();
                      formData.append('file', (<HTMLInputElement>document.getElementById("file")).files[0]);
                      this.httpClient.post(result.data.url, formData, {
                        observe: 'events',
                        reportProgress: true
                      }).subscribe((result) => {
                          //events of upload
                      });

```


Upload files to s3 throug a presigned url


### Form Parameters

Parameter | Required | type | Description
--------- | ------- | --------|-----------
key | true | string | name  of object to store on s3 this must be same and it is from POST Application API
AWSAccessKeyId | true | string | From POST Application API
policy | true | string | From POST Application API
signature | true | string | From POST Application API
x-amz-security-token | true | string | From POST Application API
file | true | string | base64 file

### Response 

201 status response

### Test with Swagger

No Swagger API , this endpoind hits directly to AWS s3


# Request AnyVision validation

## POST Request AnyVision

```typescript
// GET All countries
postValidateImages(): Observable<IValidateResponse[]>{
   const params = new HttpParams();
   return this.httpClient.request(
     'POST', 
     `${this.urlBase}/validate`, 
     {responseType:'json' , Authorization : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjMyIiwiaWF0IjoxNjMzNDgzNTI2LCJleHAiOjE2MzM0ODM1ODZ9.6_EZjvsV9kFLKKNfC59H4R7CXlFVM6TZufXNZdYCyV0' });
}
```

> The above command returns JSON structured like this:

```json
// 200
{
  "id": "615488b2dbcb87c08e240fae", 
  "score": 0.555
}


// 400
{
  "message": "IMAGES.DONTMATCH",
  "error": "Bad Request"
}

//Message for invalid document ID
{
  "message": "ANYVISION.INVALIDID",
  "error": "Bad Request"
}

//Message for invalid photo
{
  "message": "ANYVISION.INVALIDPHOTO",
  "error": "Bad Request"
}

//User replication
{
  "message": "ANYVISION.USERREPLICATION",
  "error": "Bad Request"
}


//500
{
  "message": "INTERNAL.SERVERERROR",
  "error": "Internal server error"
}
```

This endpoint validate identifier file vs photo
### Headers

Authorization - token from POST application

### HTTP Request

`POST https://development-api-argobot.umvel.dev/api/validate`     
`POST https://staging-api-argobot.umvel.dev:3002/api/validate`    
`POST https://demo-api-argobot.umvel.dev:3003/api/validate`  

### Query Parameters

**None**

### Response 200

Parameter | type | Description
--------- | --------|-----------
score | float | value of match bewtwen images
valid | boolean | Validate if score is over thredshold

### Response 400 messages

CODE | Description
--------- | -----------
IMAGES.DONTMATCH | Images does not have features
INTERNAL.SERVERERROR | There was an unexpected exception

### Response 500 messages

CODE | Description
--------- | -----------
INTERNAL.SERVERERROR | There was an unexpected exception


### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 

<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>



# Connect to websocket

## WebSocket Connection

```typescript
//example from https://www.npmjs.com/package/rxjs-websockets

import { QueueingSubject } from 'queueing-subject'
import { Subscription } from 'rxjs'
import { share, switchMap } from 'rxjs/operators'
import makeWebSocketObservable, {
  GetWebSocketResponses,
  // WebSocketPayload = string | ArrayBuffer | Blob
  WebSocketPayload,
  normalClosureMessage,
} from 'rxjs-websockets'

// this subject queues as necessary to ensure every message is delivered
const input$ = new QueueingSubject<string>()

// queue up a request to be sent when the websocket connects
input$.next('some data')

// create the websocket observable, does *not* open the websocket connection
// token comes from POST application proccess
const socket$ = makeWebSocketObservable(`ws://localhost/websocket-path?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjMyIiwiaWF0IjoxNjMzNDgzNTI2LCJleHAiOjE2MzM0ODM1ODZ9.6_EZjvsV9kFLKKNfC59H4R7CXlFVM6TZufXNZdYCyV0`)

const messages$: Observable<WebSocketPayload> = socket$.pipe(
  // the observable produces a value once the websocket has been opened
  switchMap((getResponses: GetWebSocketResponses) => {
    console.log('websocket opened')
    return getResponses(input$)
  }),
  share(),
)

const messagesSubscription: Subscription = messages.subscribe(
  (message: string) => {
    console.log('received message:', message)
    // respond to server
    input$.next('i got your message')
  },
  (error: Error) => {
    const { message } = error
    if (message === normalClosureMessage) {
      console.log('server closed the websocket connection normally')
    } else {
      console.log('socket was disconnected due to error:', message)
    }
  },
  () => {
    // The clean termination only happens in response to the last
    // subscription to the observable being unsubscribed, any
    // other closure is considered an error.
    console.log('the connection was closed in response to the user')
  },
)

function closeWebsocket() {
  // this also caused the websocket connection to be closed
  messagesSubscription.unsubscribe()
}

setTimeout(closeWebsocket, 2000)


//example of events
{
  "type":"QUESTION",
  "question_code": "615488b2dbcb87c08e240fae"
}

{
  "type":"ENDINTERVIEW",
  "question_code": "615488b2dbcb87c08e240fae"
}

//example of message connection
//200
{
   "message":"OK"
}
//400
{
   "message":"UNAUTHORIZED"
}
//500
{
   "message":"INTERNALSERVERERROR"
}

```

Connect to websocket to receive messages from Engine of inference


### HTTP Request

 `wss://41lcvtgt9k.execute-api.us-west-1.amazonaws.com/dev`

### Query Parameters

Parameter | Required | type | Description
--------- | ------- | --------|-----------
token | true | string | token from POST application proccess

### Response connection 200

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string | OK


### Response connection 400

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string |  UNAUTHORIZED

### Response connection 500

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string | INTERNALSERVERERROR

### Type of events
Code | Description
-------| -----------------
QUESTION | Event of type question
ENDINTERVIEW  | Event end of interview 


### Events 
Parameter | Required | Type | Description
--------- | ------- | --------|-----------
type | true | string | type of event question
question_code |  conditional | string | Id of question(requiered when type of event is question)


<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>



# Question

## GET Request question information

```typescript
export interface IQuestion {
  id:string;
  text:string;
  audioUrl:string;
}
// GET All countries
getRequestQuestionInformation(): Observable<IQuestion[]>{
   const params = new HttpParams({
     
   });
   return this.httpClient.request(
     'GET', 
     `${this.urlBase}/question/${questionId}`, 
     {responseType:'json' , "Authorization" : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjMyIiwiaWF0IjoxNjMzNDgzNTI2LCJleHAiOjE2MzM0ODM1ODZ9.6_EZjvsV9kFLKKNfC59H4R7CXlFVM6TZufXNZdYCyV0' });
}
```

> The above command returns JSON structured like this:

```json
//200
{
   "id":"615488b2dbcb87c08e240fae",
   "text":" What is your favorite food",
   "audioUrl":"https://argo-bot-files-dev.s3.us-west-1.amazonaws.com/salli/Argobot-1.mp3"
}
```

> The above command returns JSON structured like this:

```json

//400
{
  "message": "QUESTION.ITEMNOTFOUND",
  "error": "Bad Request"
}
```
> The above command returns JSON structured like this:

```json
//500
{
  "message": "INTERVAL.SERVERERROR",
  "error": "Bad Request"
}
```

This endpoint retrieves a information of question 

### Headers

Authorization - token from POST application

### HTTP Request
 
`GET https://development-api-argobot.umvel.dev/api/question/question_code`     
`GET https://staging-api-argobot.umvel.dev:3002/api/question/question_code`    
`GET https://demo-api-argobot.umvel.dev:3003/api/question/question_code`  


### Query Parameters

Parameter | Required | Type | Description
--------- | ------- | --------|-----------
question_code |  true | string | Id of question

### Response 400 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string |  QUESTION.ITEMNOTFOUND

### Response 500 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string | INTERVAL.SERVERERROR


### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 


<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Connect to AWS transcribe 

## Specifiations
This service must be used only on chat component

```typescript

export interface ISpeechItem {
  content:string;
  endTime:number;
  startTime: number;
  type: string; "pronunciation" | "puntuation";
  vocabularyFilterMatch: boolean;
  confidence: number; // 0 < x < 1
}

export class HomeComponent implements OnInit {
  isEditable = false;
  isLinear = true;
  //TODO: Remove access to AWSTranscribeService is only with test porpouses
  constructor(private awsTranscribeService: AwsTranscribeService) {
    this.awsTranscribeService.startRecording().subscribe((message: ISpeechItem) => console.log(message), err => console.log(err));
    setTimeout(() => {
      this.awsTranscribeService.stopRecording();
    }, 20000);
  }

  ngOnInit(): void {
  }
}
```


# Answer

## POST Answer

```typescript

export interface ISpeech {
  items:Array<ISpeechItem>;
  text: string;
}

export interface IPostAnswer {
  questionId:string;
  answer:ISpeech;
}

export interface ISpeechItem {
  content:string;
  endTime:number;
  startTime: number;
  type: string; "pronunciation" | "puntuation";
  vocabularyFilterMatch: boolean;
  confidence: number; // 0 < x < 1
}


export interface IAnswer {
  id :string;
}

// Save answer
postAnswer(): Observable<IAnswer[]>{
   const params = new HttpParams(
     {
       questionId :"615488b2dbcb87c08e240fae",
       answer: items,//items from speech
     }
   );
   return this.httpClient.request(
     'POST', 
     `${this.urlBase}/answer`, 
     {responseType:'json' , "Authorization" : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjMyIiwiaWF0IjoxNjMzNDgzNTI2LCJleHAiOjE2MzM0ODM1ODZ9.6_EZjvsV9kFLKKNfC59H4R7CXlFVM6TZufXNZdYCyV0' });
}
```

> The above command returns JSON structured like this:

```json
//200
{
  "id":"615488b2dbcb87c08e240fae"
}
```

> The above command returns JSON structured like this:

```json

//400
{
  "message": "ANSWER.ANSWERTOOLONG",
  "error": "Bad Request"
}
```
> The above command returns JSON structured like this:

```json
//500
{
  "message": "INTERVAL.SERVERERROR",
  "error": "Bad Request"
}
```

Use to send an answer to backend

### Headers

Authorization - token from POST application

### HTTP Request

`POST https://development-api-argobot.umvel.dev/api/answer`     
`POST https://staging-api-argobot.umvel.dev:3002/api/answer`    
`POST https://demo-api-argobot.umvel.dev:3003/api/answer`  

### Body Parameters

Parameter | Required | Type | Description
--------- | ------- | --------|-----------
answer |  true | ISpeech | information about speech 
questionId | true | string | id of question id 

### Response 400 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string |  ANSWER.ANSWERTOOLONG
message | true | string |  ANSWER.NOTVALID

### Response 500 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string | INTERVAL.SERVERERROR

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 


<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Results

## GET Results

```typescript

export interface IQuestionResult{
  score : number;
  question: string;
}

export interface IResult {
  score:number;
  questions:Array<IQuestionResult>;
}

// Get results
postAnswer(): Observable<IResult[]>{
   const params = new HttpParams(
   );
   return this.httpClient.request(
     'POST', 
     `${this.urlBase}/result`, 
     {responseType:'json' , "Authorization" : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjMyIiwiaWF0IjoxNjMzNDgzNTI2LCJleHAiOjE2MzM0ODM1ODZ9.6_EZjvsV9kFLKKNfC59H4R7CXlFVM6TZufXNZdYCyV0' });
}
```

> The above command returns JSON structured like this:

```json
//200
{
  "result_score": 90,
  "result_description": "Approved",
  "criteria": [
    {
      "score": 40,
      "criteria": "Born USA"
    },
    {
      "score": 50,
      "criteria": "Marriage USA"
    }
  ]
}
```
> The above command returns JSON structured like this:

```json

//400
{
  "message": "RESULT.INFORMATIONNOTFOUND",
  "error": "Bad Request"
}
```
> The above command returns JSON structured like this:

```json
//500
{
  "message": "INTERVAL.SERVERERROR",
  "error": "Bad Request"
}
```

Use to get results of application

### Headers

Authorization - token from POST application

### HTTP Request

`GET https://development-api-argobot.umvel.dev/api/result`     
`GET https://staging-api-argobot.umvel.dev:3002/api/result`    
`GET https://demo-api-argobot.umvel.dev:3003/api/result`  


### Body Parameters

### Response 400 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string |  RESULT.INFORMATIONNOTFOUND

### Response 500 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string | INTERVAL.SERVERERROR

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 


<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>

# Face from document ID

## GET Face

```typescript

export interface IFace{
  presignedUrl:string;
}

// Save answer
getFace(): Observable<>{
   const params = new HttpParams(
   );
   return this.httpClient.request(
     'GET', 
     `${this.urlBase}/face`, 
     {responseType:'json' , "Authorization" : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjMyIiwiaWF0IjoxNjMzNDgzNTI2LCJleHAiOjE2MzM0ODM1ODZ9.6_EZjvsV9kFLKKNfC59H4R7CXlFVM6TZufXNZdYCyV0' });
}
```

> The above command returns JSON structured like this:

```json
//200
{
  "presignedUrl":"https://us-east-1.amazon.com/s3/file.jpg"
}

//400
{
  "message": "FACE.INVALIDID",
  "error": "Bad Request"
}
```

Use to extract face from document ID

### Headers

Authorization - token from POST application

### HTTP Request

`POST https://development-api-argobot.umvel.dev/api/face`     
`POST https://staging-api-argobot.umvel.dev:3002/api/face`    
`POST https://demo-api-argobot.umvel.dev:3003/api/face`  

### Body Parameters

Parameter | Required | Type | Description
--------- | ------- | --------|-----------

### Response 400 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string |  FACE.INVALIDID

### Response 500 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string | INTERVAL.SERVERERROR

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 


<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>


# Feedback

## POST feedback

```typescript

export interface IFeedBack{
  score:number; //  1 | 2 | 3 | 4 | 5
  description:string; 
}

// Save answer
postFeedBack(): Observable<boolean>{
   const params = new HttpParams(
   );
   return this.httpClient.request(
     'POST', 
     `${this.urlBase}/feedback`, 
     {responseType:'json' , "Authorization" : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjMyIiwiaWF0IjoxNjMzNDgzNTI2LCJleHAiOjE2MzM0ODM1ODZ9.6_EZjvsV9kFLKKNfC59H4R7CXlFVM6TZufXNZdYCyV0' });
}
```

> The above command returns JSON structured like this:

```json
//200
{
  "id":"123qweqweqwe"
}

```

Send feedbacl to backend

### Headers

Authorization - token from POST application

### HTTP Request

`POST https://development-api-argobot.umvel.dev/api/feedback`     
`POST https://staging-api-argobot.umvel.dev:3002/api/feedback`    
`POST https://demo-api-argobot.umvel.dev:3003/api/feedback`  

### Body Parameters

Parameter | Required | Type | Description
--------- | ------- | --------|-----------
score  | true  | number | value betwen 1 and 5 
description  | true | string | Aditional information of feedback

### Response 400 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------

### Response 500 codes

Parameter | Required | Type | value
--------- | ------- | --------|-----------
message | true | string | INTERVAL.SERVERERROR

### Test with Swagger

[ development ](https://development-api-argobot.umvel.dev/api)    
[ staging ](https://staging-api-argobot.umvel.dev:3002/api)    
[ demo ](https://demo-api-argobot.umvel.dev:3003/api) 


<aside class="warning">
Remember — This endpoint only can be call from web browsers
</aside>





# Audio files


URL | name | environment | description
--------- |--------|-----------|-----------
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-1.mp3 | Salli-Argobot-1.mp3 | dev | Hello! I'm argo boot ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-1.mp3 | Salli-Argobot-1.mp3 | stg | Hello! I'm argo boot ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-1.mp3 | Salli-Argobot-1.mp3 | demo | Hello! I'm argo boot ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-2.mp3 | Salli-Argobot-2.mp3 | dev | That it's great welcome to practice interview  ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-2.mp3 | Salli-Argobot-2.mp3 | stg | That it's great welcome to practice interview  ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-2.mp3 | Salli-Argobot-2.mp3 | demo | That it's great welcome to practice interview  ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-3.mp3 | Salli-Argobot-3.mp3 | dev | In order to continue with the interview ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-3.mp3 | Salli-Argobot-3.mp3 | stg | In order to continue with the interview ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-3.mp3 | Salli-Argobot-3.mp3 | demo | In order to continue with the interview ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-4.mp3 | Salli-Argobot-4.mp3 | dev | Nice to see you. To verify your identity I will need ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-4.mp3 | Salli-Argobot-4.mp3 | stg | Nice to see you. To verify your identity I will need ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-4.mp3 | Salli-Argobot-4.mp3 | demo | Nice to see you. To verify your identity I will need ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-5.mp3 | Salli-Argobot-5.mp3 | dev | Everything is good , your identity has been ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-5.mp3 | Salli-Argobot-5.mp3 | stg | Everything is good , your identity has been ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-5.mp3 | Salli-Argobot-5.mp3 | demo | Everything is good , your identity has been ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-6.mp3 | Salli-Argobot-6.mp3 | dev | I can't match your information ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-6.mp3 | Salli-Argobot-6.mp3 | stg | I can't match your information ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-6.mp3 | Salli-Argobot-6.mp3 | demo | I can't match your information ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-7.mp3 | Salli-Argobot-7.mp3 | dev | We are almost there. Just let me check other ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-7.mp3 | Salli-Argobot-7.mp3 | stg | We are almost there. Just let me check other ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-7.mp3 | Salli-Argobot-7.mp3 | demo | We are almost there. Just let me check other ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-8.mp3 | Salli-Argobot-8.mp3 | dev | I am sorry but it is not possible to continue ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-8.mp3 | Salli-Argobot-8.mp3 | stg | I am sorry but it is not possible to continue ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-8.mp3 | Salli-Argobot-8.mp3 | demo | I am sorry but it is not possible to continue ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-9.mp3 | Salli-Argobot-9.mp3 | dev | Perfect! Lastly, I will do a quick check ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-9.mp3 | Salli-Argobot-9.mp3 | stg | Perfect! Lastly, I will do a quick check ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-9.mp3 | Salli-Argobot-9.mp3 | demo | Perfect! Lastly, I will do a quick check ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-10.mp3 | Salli-Argobot-10.mp3 | dev | I could not match your information with the photo ID ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-10.mp3 | Salli-Argobot-10.mp3 | stg | I could not match your information with the photo ID ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-10.mp3 | Salli-Argobot-10.mp3 | demo | I could not match your information with the photo ID ...
 https://argo-bot-files-dev.umvel.dev/Salli/Salli-Argobot-11.mp3 | Salli-Argobot-11.mp3 | dev | I am sorry but it is not possible to continue to the ...
 https://argo-bot-files-stg.umvel.dev/Salli/Salli-Argobot-11.mp3 | Salli-Argobot-11.mp3 | stg | I am sorry but it is not possible to continue to the ...
 https://argo-bot-files-demo.umvel.dev/Salli/Salli-Argobot-11.mp3 | Salli-Argobot-11.mp3 | demo | I am sorry but it is not possible to continue to the ...

# Status Code

```typescript
// Manage error response

```
> The above command returns JSON structured like this:

```json
{
  "statusCode": 400,
  "message": [
    "KEY1",
    "KEY2"
  ],
  "error": "Bad Request"
}

{
  "statusCode": 500,
  "message": [
    "KEY1"
  ],
  "error": "Internal server error"
}


{
  "statusCode": 404,
  "message": [
    "KEY1"
  ],
  "error": "Internal server error"
}
```
Information about type of response 

## Response 400 BadRequest 

Status Code  | Description
--------- | -----------
400 |  Bad request 
500   | Internal server error
404 | Not found 
200 | OK